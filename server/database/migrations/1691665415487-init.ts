import { MigrationInterface, QueryRunner } from "typeorm";

export class init1691665415487 implements MigrationInterface {
    name = 'init1691665415487'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "public"."assets" ("id" SERIAL NOT NULL, "tv" boolean NOT NULL DEFAULT false, "bed" boolean NOT NULL DEFAULT false, "fridge" boolean NOT NULL DEFAULT false, "wc" boolean NOT NULL DEFAULT false, "kitchen" boolean NOT NULL DEFAULT false, "waterHeater" boolean NOT NULL DEFAULT false, "washingMachine" boolean NOT NULL DEFAULT false, CONSTRAINT "PK_bbd9b67466ce36ece4b2f1e4fd1" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "public"."room" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "floor" integer NOT NULL, "capacity" integer NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "assetsId" integer, CONSTRAINT "REL_f751c08da96ff05ee468b3c21d" UNIQUE ("assetsId"), CONSTRAINT "PK_31f36b1dc3711f6c4939d0dac63" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "public"."customer" ("id" SERIAL NOT NULL, "firstName" character varying NOT NULL, "lastName" character varying NOT NULL, "email" character varying NOT NULL, "phone" character varying NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_493862f6fb77845712126f204eb" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "public"."booking" ("id" SERIAL NOT NULL, "bookingStartDate" TIMESTAMP WITH TIME ZONE NOT NULL, "bookingEndDate" TIMESTAMP WITH TIME ZONE NOT NULL, "purpose" character varying NOT NULL, "bussinessUnit" character varying NOT NULL, "roomId" integer, "customerId" integer, CONSTRAINT "PK_ff44e24f80a67eab6bb12ed18a5" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "public"."room" ADD CONSTRAINT "FK_f751c08da96ff05ee468b3c21dc" FOREIGN KEY ("assetsId") REFERENCES "public"."assets"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."booking" ADD CONSTRAINT "FK_b23ebfa3b982b418f69ab24f140" FOREIGN KEY ("roomId") REFERENCES "public"."room"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."booking" ADD CONSTRAINT "FK_4a77d172a396a20275ab3d71afc" FOREIGN KEY ("customerId") REFERENCES "public"."customer"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."booking" DROP CONSTRAINT "FK_4a77d172a396a20275ab3d71afc"`);
        await queryRunner.query(`ALTER TABLE "public"."booking" DROP CONSTRAINT "FK_b23ebfa3b982b418f69ab24f140"`);
        await queryRunner.query(`ALTER TABLE "public"."room" DROP CONSTRAINT "FK_f751c08da96ff05ee468b3c21dc"`);
        await queryRunner.query(`DROP TABLE "public"."booking"`);
        await queryRunner.query(`DROP TABLE "public"."customer"`);
        await queryRunner.query(`DROP TABLE "public"."room"`);
        await queryRunner.query(`DROP TABLE "public"."assets"`);
    }

}
