import { Room } from 'src/domain/entities/room.entity';
import { BookingRepository } from 'src/domain/repositories/bookingRepository.interface';
import { RoomRepository } from 'src/domain/repositories/roomRepository.interface';

interface GetRoomsAvailablePayload {
  from: Date;
  to: Date;
}

export interface RoomsAvailable {
  day: Date;
  rooms: Room[];
}

export class GetRoomsAvailableUseCases {
  constructor(
    private readonly bookingRepository: BookingRepository,
    private readonly roomRepository: RoomRepository,
  ) { }

  async execute(payload: GetRoomsAvailablePayload): Promise<RoomsAvailable[]> {
    const rooms = await this.roomRepository.findAll();
    const arrayDays = this.getDatesBetween(payload.from, payload.to);
    const result = await Promise.all(
      arrayDays.map(async day => {
        const booking = await this.bookingRepository.getRoomsNotAvailable(this.getStartDay(day), this.getEndDay(day));
        const mapRoom = new Map<number, Room>();
        booking.forEach(item => {
          mapRoom.set(item.room.id, item.room);
        });
        const bookedRooms = Array.from(mapRoom.values());
        return {
          day,
          rooms: rooms.filter(room => !bookedRooms.map(bookedRoom => bookedRoom.id).includes(room.id)),
        }
      }),
    );
    return result;
  }

  private getDatesBetween(startDate: Date, endDate: Date): Date[] {
    const currentDate = new Date(startDate.getTime());
    const dates: Date[] = [];
    while (currentDate <= endDate) {
      dates.push(new Date(currentDate));
      currentDate.setDate(currentDate.getDate() + 1);
    }
    return dates;
  }

  private getStartDay(date: Date) {
    const cloneDate = new Date(date.toLocaleString('en', { timeZone: 'Asia/Ho_Chi_Minh' }));
    cloneDate.setUTCHours(0, 0, 0, 0);
    return cloneDate;
  }

  private getEndDay(date: Date) {
    const cloneDate = new Date(date.toLocaleString('en', { timeZone: 'Asia/Ho_Chi_Minh' }));
    cloneDate.setUTCHours(23, 59, 59, 999);
    return cloneDate;
  }
}
