import { BookingRepository, RoomsBookedByCustomerId } from 'src/domain/repositories/bookingRepository.interface';
import { CustomerRepository } from 'src/domain/repositories/customerRepository.interface';

interface GetRoomsBookedByCustomerIdPayload {
  customerId: number;
  from?: Date;
  to?: Date;
}

export class GetRoomsBookedByCustomerIdUseCases {
  constructor(
    private readonly bookingRepository: BookingRepository,
    private readonly customerRepository: CustomerRepository,
  ) { }

  async execute(payload: GetRoomsBookedByCustomerIdPayload): Promise<RoomsBookedByCustomerId[]> {
    const { customerId } = payload;
    const customer = await this.customerRepository.findById(customerId);

    if (!customer) {
      throw new Error('Customer not found');
    }

    return await this.bookingRepository.getRoomsBookedByCustomerId(payload);
  }
}
