import { Booking } from 'src/domain/entities/booking.entity';
import { BookingRepository } from 'src/domain/repositories/bookingRepository.interface';
import { CustomerRepository } from 'src/domain/repositories/customerRepository.interface';
import { RoomRepository } from 'src/domain/repositories/roomRepository.interface';
import { ILogger } from '../../domain/logger/logger.interface';

interface BookingPayload {
  roomId: number;
  customerId: number;

  bookingStartDate: Date;
  bookingEndDate: Date;
  purpose: string;
  bussinessUnit: string
}

export class BookingRoomsUseCases {
  constructor(
    private readonly logger: ILogger,
    private readonly bookingRepository: BookingRepository,
    private readonly roomRepository: RoomRepository,
    private readonly customerRepository: CustomerRepository,
  ) { }

  async execute(payload: BookingPayload[]): Promise<Booking[]> {
    const arrayCustomerId = payload.map(item => item.customerId);
    const arrayRoomId = payload.map(item => item.roomId);
    const mapCustomerId = new Set(arrayCustomerId);
    const mapRoomId = new Set(arrayRoomId);

    const [customers, rooms] = await Promise.all([
      this.customerRepository.findByIds([...mapCustomerId]),
      this.roomRepository.findByIds([...mapRoomId]),
    ]);

    // check booking available
    const isAvailable = await Promise.all(
      payload.map(item => this.bookingRepository.isBookingAvailable(item.roomId, item.bookingStartDate, item.bookingEndDate)),
    );

    if (isAvailable.some(item => !item)) {
      throw new Error('Room not available');
    }

    // check duplicates input
    const sortedPayload = payload.sort((a, b) => a.roomId - b.roomId);

    for (let i = 0; i < sortedPayload.length - 1; i++) {
      if (sortedPayload[i].roomId === sortedPayload[i + 1].roomId) {
        if ((sortedPayload[i].bookingStartDate <= sortedPayload[i + 1].bookingStartDate || sortedPayload[i].bookingStartDate <= sortedPayload[i + 1].bookingEndDate) &&
          sortedPayload[i].bookingEndDate >= sortedPayload[i + 1].bookingStartDate || sortedPayload[i].bookingEndDate >= sortedPayload[i + 1].bookingEndDate) {
          throw new Error('Input booking is invalid');
        }
      }
    }

    payload.map(item => {
      const customer = customers.find(customer => customer.id === item.customerId);
      const room = rooms.find(room => room.id === item.roomId);
      if (!customer) {
        throw new Error('Customer not found');
      }

      if (!room) {
        throw new Error('Room not found');
      }

      return this.bookingRepository.create({ ...item, customer, room });
    })

    const bookings = await Promise.all(
      payload.map(item => {
        const customer = customers.find(customer => customer.id === item.customerId);
        const room = rooms.find(room => room.id === item.roomId);
        return this.bookingRepository.create({ ...item, customer, room });
      }),
    );

    this.logger.log('BookingRoomsUseCases execute', 'The room has been booked');
    return bookings;
  }
}
