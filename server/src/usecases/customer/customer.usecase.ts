import { Customer } from 'src/domain/entities/customer.entity';
import { CustomerRepository } from 'src/domain/repositories/customerRepository.interface';
import { ILogger } from '../../domain/logger/logger.interface';

export class CreateCustomerUseCases {
    constructor(private readonly logger: ILogger, private readonly cutomerRepository: CustomerRepository) { }

    async execute(customer: Omit<Customer, 'id'>): Promise<Customer> {
        const result = await this.cutomerRepository.create(customer);
        this.logger.log('CreateCustomerUseCases execute', 'New customer have been created');
        return result;
    }
}

export class DeleteCustomerUseCases {
    constructor(private readonly logger: ILogger, private readonly cutomerRepository: CustomerRepository) { }

    async execute(id: number): Promise<void> {
        await this.cutomerRepository.deleteById(id);
        this.logger.log('DeleteRoomUseCases execute', `Room ${id} have been deleted`);
    }
}

export class GetCustomerUseCases {
    constructor(private readonly cutomerRepository: CustomerRepository) { }

    async execute(id: number): Promise<Customer> {
        return await this.cutomerRepository.findById(id);
    }
}

