import { RoomRepository } from 'src/domain/repositories/roomRepository.interface';
import { ILogger } from '../../domain/logger/logger.interface';
import { Room } from 'src/domain/entities/room.entity';
import { AssetsRepository } from 'src/domain/repositories/assetsRepository.interface';

interface RoomPayload {
  name: string;
  floor: number;
  capacity: number;
  tv?: boolean;
  bed?: boolean;
  fridge?: boolean;
  wc?: boolean;
  kitchen?: boolean;
  waterHeater?: boolean;
  washingMachine?: boolean;
}

interface UpdateRoomPayload {
  id: number;
  name?: string;
  floor?: number;
  capacity?: number;
}

export class AddRoomUseCases {
  constructor(
    private readonly logger: ILogger,
    private readonly roomRepository: RoomRepository,
    private readonly assetRepository: AssetsRepository,
  ) { }

  async execute(payload: RoomPayload): Promise<Room> {
    const { name, floor, capacity, ...assets } = payload;
    const assetsEnity = await this.assetRepository.create(assets);
    const result = await this.roomRepository.insert({
      name, floor, capacity, assets: assetsEnity
    });
    this.logger.log('AddRoomUseCases execute', 'New room have been inserted');
    return result;
  }
}

export class DeleteRoomUseCases {
  constructor(
    private readonly logger: ILogger,
    private readonly roomRepository: RoomRepository,
    private readonly assetsRepository: AssetsRepository,
  ) { }

  async execute(id: number): Promise<void> {
    const room = await this.roomRepository.findById(id);
    await Promise.all([
      this.assetsRepository.deleteById(room.assets.id),
      this.roomRepository.deleteById(id),
    ]);
    this.logger.log('DeleteRoomUseCases execute', `Room ${id} have been deleted`);
  }
}

export class GetRoomUseCases {
  constructor(private readonly roomRepository: RoomRepository) { }

  async execute(id: number): Promise<Room> {
    return await this.roomRepository.findById(id);
  }
}

export class GetRoomsUseCases {
  constructor(private readonly roomRepository: RoomRepository) { }

  async execute(): Promise<Room[]> {
    return await this.roomRepository.findAll();
  }
}

export class UpdateRoomUseCases {
  constructor(
    private readonly logger: ILogger,
    private readonly roomRepository: RoomRepository,
  ) { }

  async execute(payload: UpdateRoomPayload): Promise<void> {
    const { id } = payload;
    await this.roomRepository.updateById(id, payload);
    this.logger.log('UpdateRoomUseCases execute', 'the room have been updated');
  }
}

