import { Column, CreateDateColumn, Entity, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { CustomerModel } from './customer.model';
import { RoomModel } from './room.model';

@Entity('assets')
export class AssetsModel {
    @PrimaryGeneratedColumn({ type: 'integer' })
    id: number;

    @Column('bool', { default: false })
    tv: boolean;

    @Column('bool', { default: false })
    bed: boolean;

    @Column('bool', { default: false })
    fridge: boolean;

    @Column('bool', { default: false })
    wc: boolean;

    @Column('bool', { default: false })
    kitchen: boolean;

    @Column('bool', { default: false })
    waterHeater: boolean;

    @Column('bool', { default: false })
    washingMachine: boolean;
}
