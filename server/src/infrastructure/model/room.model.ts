import { Column, CreateDateColumn, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { AssetsModel } from './assets.model';
import { BookingModel } from './booking.model';

@Entity('room')
export class RoomModel {
  @PrimaryGeneratedColumn({ type: 'integer' })
  id: number;

  @Column('varchar')
  name: string;

  @Column('integer')
  floor: number;

  @Column('integer')
  capacity: number;

  @OneToOne(() => AssetsModel)
  @JoinColumn()
  assets: AssetsModel;

  @OneToMany(() => BookingModel, (bookingModel) => bookingModel.room, { cascade: true })
  bookings: BookingModel[];

  @CreateDateColumn()
  createdDate?: Date;

  @UpdateDateColumn()
  updatedDate?: Date;
}
