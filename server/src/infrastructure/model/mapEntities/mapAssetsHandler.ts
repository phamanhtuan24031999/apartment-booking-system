import { Assets } from "src/domain/entities/assets.entity";
import { AssetsModel } from "../assets.model";

export class MapAssetsHandler {
    toEntity(model: AssetsModel): Assets {
        return {
            id: model.id,
            tv: model.tv,
            bed: model.bed,
            fridge: model.fridge,
            wc: model.wc,
            kitchen: model.kitchen,
            waterHeater: model.waterHeater,
            washingMachine: model.washingMachine,
        }
    }
}