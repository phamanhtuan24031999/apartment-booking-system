import { Customer } from "src/domain/entities/customer.entity";
import { CustomerModel } from "../customer.model";

export class MapCustomerHandler {
    toEntity(model: CustomerModel): Customer {
        return {
            id: model.id,
            firstName: model.firstName,
            lastName: model.lastName,
            email: model.email,
            phone: model.phone,
            createdDate: model.createdDate,
            updatedDate: model.updatedDate,
        }
    }
}