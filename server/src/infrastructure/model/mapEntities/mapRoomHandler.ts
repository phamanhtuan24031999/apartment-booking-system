import { Room } from "src/domain/entities/room.entity";
import { RoomModel } from "../room.model";
import { MapAssetsHandler } from "./mapAssetsHandler";
import { MapBookingHandler } from "./mapBookingHandler";

export class MapRoomHandler {
    mapAssetsHandler: MapAssetsHandler = new MapAssetsHandler();
    mapBookingHandler: MapBookingHandler = new MapBookingHandler();
    toEntity(model: RoomModel): Room {
        return {
            id: model.id,
            name: model.name,
            floor: model.floor,
            capacity: model.capacity,
            assets: this.mapAssetsHandler.toEntity(model.assets),
            createdDate: model.createdDate,
            updatedDate: model.updatedDate
        }
    }
}