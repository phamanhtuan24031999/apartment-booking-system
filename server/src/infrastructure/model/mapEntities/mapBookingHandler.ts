import { Booking } from "src/domain/entities/booking.entity";
import { BookingModel } from "../booking.model";

export class MapBookingHandler {
    toEntity(model: BookingModel): Booking {
        return {
            id: model.id,
            room: model.room,
            customer: model.customer,
            bookingStartDate: model.bookingStartDate,
            bookingEndDate: model.bookingEndDate,
            bussinessUnit: model.bussinessUnit,
            purpose: model.purpose,
        }
    }
}