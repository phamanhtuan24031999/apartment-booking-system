import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { CustomerModel } from './customer.model';
import { RoomModel } from './room.model';

@Entity('booking')
export class BookingModel {
    @PrimaryGeneratedColumn({ type: 'integer' })
    id: number;

    @Column('timestamptz')
    bookingStartDate: Date;

    @Column('timestamptz')
    bookingEndDate: Date;

    @Column('varchar')
    purpose: string;

    @Column('varchar')
    bussinessUnit: string;

    @ManyToOne(() => RoomModel, (roomModel) => roomModel.bookings)
    room: RoomModel;

    @ManyToOne(() => CustomerModel, (customerModel) => customerModel.bookings)
    customer: CustomerModel;
}
