import { DynamicModule, Module } from '@nestjs/common';
import { AddRoomUseCases, DeleteRoomUseCases, GetRoomUseCases, GetRoomsUseCases, UpdateRoomUseCases } from '../../usecases/room/room.usecases';

import { ExceptionsModule } from '../exceptions/exceptions.module';
import { LoggerModule } from '../logger/logger.module';
import { LoggerService } from '../logger/logger.service';

import { RepositoriesModule } from '../repositories/repositories.module';

import { DatabaseRoomRepository } from '../repositories/room.repository';

import { EnvironmentConfigModule } from '../config/environment-config/environment-config.module';
import { UseCaseProxy } from './usecases-proxy';
import { DatabaseCustomerRepository } from '../repositories/customer.repository';
import { CreateCustomerUseCases, DeleteCustomerUseCases, GetCustomerUseCases } from 'src/usecases/customer/customer.usecase';
import { DatabaseAssetsRepository } from '../repositories/assets.repository';
import { BookingRoomsUseCases } from 'src/usecases/booking/bookingRooms.usecases';
import { DatabaseBookingRepository } from '../repositories/booking.repository';
import { GetRoomsBookedByCustomerIdUseCases } from 'src/usecases/booking/getRoomsBookedByCustomerId.usecases';
import { GetRoomsAvailableUseCases } from 'src/usecases/booking/getRoomsAvailable.usecases';

@Module({
  imports: [LoggerModule, EnvironmentConfigModule, RepositoriesModule, ExceptionsModule],
})
export class UsecasesProxyModule {
  // Room
  static GET_ROOM_USECASES_PROXY = 'getRoomUsecasesProxy';
  static GET_ROOMS_USECASES_PROXY = 'getRoomsUsecasesProxy';
  static POST_ROOM_USECASES_PROXY = 'postRoomUsecasesProxy';
  static DELETE_ROOM_USECASES_PROXY = 'deleteRoomUsecasesProxy';
  static PATCH_ROOM_USECASES_PROXY = 'patchRoomUsecasesProxy';

  // Customer
  static GET_CUSTOMER_USECASES_PROXY = 'getCustomerUsecasesProxy';
  static POST_CUSTOMER_USECASES_PROXY = 'postCustomerUsecasesProxy';
  static DELETE_CUSTOMER_USECASES_PROXY = 'deleteCustomerUsecasesProxy';

  // Customer
  static BOOKING_ROOM_USECASES_PROXY = 'bookingRoomUsecasesProxy';
  static GET_ROOM_BOOKED_BY_CUSTOMERID_USECASES_PROXY = 'getRoomsBookedByCustomerIdUsecasesProxy';
  static GET_ROOMS_AVAILABLE_USECASES_PROXY = 'getRoomsAvailableUsecasesProxy';

  static register(): DynamicModule {
    return {
      module: UsecasesProxyModule,
      providers: [
        // room
        {
          inject: [DatabaseRoomRepository],
          provide: UsecasesProxyModule.GET_ROOM_USECASES_PROXY,
          useFactory: (roomRepository: DatabaseRoomRepository) => new UseCaseProxy(new GetRoomUseCases(roomRepository)),
        },
        {
          inject: [DatabaseRoomRepository],
          provide: UsecasesProxyModule.GET_ROOMS_USECASES_PROXY,
          useFactory: (roomRepository: DatabaseRoomRepository) => new UseCaseProxy(new GetRoomsUseCases(roomRepository)),
        },
        {
          inject: [LoggerService, DatabaseRoomRepository, DatabaseAssetsRepository],
          provide: UsecasesProxyModule.POST_ROOM_USECASES_PROXY,
          useFactory: (logger: LoggerService, roomRepository: DatabaseRoomRepository, assetsRepository: DatabaseAssetsRepository) =>
            new UseCaseProxy(new AddRoomUseCases(logger, roomRepository, assetsRepository)),
        },
        {
          inject: [LoggerService, DatabaseRoomRepository, DatabaseAssetsRepository],
          provide: UsecasesProxyModule.DELETE_ROOM_USECASES_PROXY,
          useFactory: (logger: LoggerService, roomRepository: DatabaseRoomRepository, assetsRepository: DatabaseAssetsRepository) =>
            new UseCaseProxy(new DeleteRoomUseCases(logger, roomRepository, assetsRepository)),
        },
        {
          inject: [LoggerService, DatabaseRoomRepository],
          provide: UsecasesProxyModule.PATCH_ROOM_USECASES_PROXY,
          useFactory: (logger: LoggerService, roomRepository: DatabaseRoomRepository) =>
            new UseCaseProxy(new UpdateRoomUseCases(logger, roomRepository)),
        },
        // customer
        {
          inject: [DatabaseCustomerRepository],
          provide: UsecasesProxyModule.GET_CUSTOMER_USECASES_PROXY,
          useFactory: (customerRepository: DatabaseCustomerRepository) => new UseCaseProxy(new GetCustomerUseCases(customerRepository)),
        },
        {
          inject: [LoggerService, DatabaseCustomerRepository],
          provide: UsecasesProxyModule.POST_CUSTOMER_USECASES_PROXY,
          useFactory: (logger: LoggerService, customerRepository: DatabaseCustomerRepository) =>
            new UseCaseProxy(new CreateCustomerUseCases(logger, customerRepository)),
        },
        {
          inject: [LoggerService, DatabaseCustomerRepository],
          provide: UsecasesProxyModule.DELETE_CUSTOMER_USECASES_PROXY,
          useFactory: (logger: LoggerService, customerRepository: DatabaseCustomerRepository) =>
            new UseCaseProxy(new DeleteCustomerUseCases(logger, customerRepository)),
        },
        // booking
        {
          inject: [LoggerService, DatabaseBookingRepository, DatabaseRoomRepository, DatabaseCustomerRepository],
          provide: UsecasesProxyModule.BOOKING_ROOM_USECASES_PROXY,
          useFactory: (logger: LoggerService, bookingRepository: DatabaseBookingRepository, roomRepository: DatabaseRoomRepository, customerRepository: DatabaseCustomerRepository) =>
            new UseCaseProxy(new BookingRoomsUseCases(logger, bookingRepository, roomRepository, customerRepository)),
        },
        {
          inject: [DatabaseBookingRepository, DatabaseCustomerRepository],
          provide: UsecasesProxyModule.GET_ROOM_BOOKED_BY_CUSTOMERID_USECASES_PROXY,
          useFactory: (bookingRepository: DatabaseBookingRepository, customerRepository: DatabaseCustomerRepository) =>
            new UseCaseProxy(new GetRoomsBookedByCustomerIdUseCases(bookingRepository, customerRepository)),
        },
        {
          inject: [DatabaseBookingRepository, DatabaseRoomRepository],
          provide: UsecasesProxyModule.GET_ROOMS_AVAILABLE_USECASES_PROXY,
          useFactory: (bookingRepository: DatabaseBookingRepository, roomRepository: DatabaseRoomRepository) =>
            new UseCaseProxy(new GetRoomsAvailableUseCases(bookingRepository, roomRepository)),
        },
      ],
      exports: [
        // room
        UsecasesProxyModule.GET_ROOM_USECASES_PROXY,
        UsecasesProxyModule.GET_ROOMS_USECASES_PROXY,
        UsecasesProxyModule.POST_ROOM_USECASES_PROXY,
        UsecasesProxyModule.DELETE_ROOM_USECASES_PROXY,
        UsecasesProxyModule.PATCH_ROOM_USECASES_PROXY,
        // customer
        UsecasesProxyModule.GET_CUSTOMER_USECASES_PROXY,
        UsecasesProxyModule.POST_CUSTOMER_USECASES_PROXY,
        UsecasesProxyModule.DELETE_CUSTOMER_USECASES_PROXY,
        // booking
        UsecasesProxyModule.BOOKING_ROOM_USECASES_PROXY,
        UsecasesProxyModule.GET_ROOM_BOOKED_BY_CUSTOMERID_USECASES_PROXY,
        UsecasesProxyModule.GET_ROOMS_AVAILABLE_USECASES_PROXY,
      ],
    };
  }
}
