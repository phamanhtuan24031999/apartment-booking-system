import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmConfigModule } from '../config/typeorm/typeorm.module';
import { DatabaseRoomRepository } from './room.repository';
import { DatabaseCustomerRepository } from './customer.repository';
import { RoomModel } from '../model/room.model';
import { BookingModel } from '../model/booking.model';
import { CustomerModel } from '../model/customer.model';
import { AssetsModel } from '../model/assets.model';
import { DatabaseBookingRepository } from './booking.repository';
import { LoggerModule } from '../logger/logger.module';
import { DatabaseAssetsRepository } from './assets.repository';

const models = [
  RoomModel,
  BookingModel,
  CustomerModel,
  AssetsModel,
];

const repositories = [
  DatabaseRoomRepository,
  DatabaseCustomerRepository,
  DatabaseBookingRepository,
  DatabaseAssetsRepository
];

@Module({
  imports: [LoggerModule, TypeOrmConfigModule, TypeOrmModule.forFeature([...models])],
  providers: [...repositories],
  exports: [...repositories],
})
export class RepositoriesModule { }
