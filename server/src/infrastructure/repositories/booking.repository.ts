import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Booking } from 'src/domain/entities/booking.entity';
import { Room } from 'src/domain/entities/room.entity';
import { BookingRepository, GetRoomsBookedByCustomerIdPayload, RoomsBookedByCustomerId } from 'src/domain/repositories/bookingRepository.interface';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { LoggerService } from '../logger/logger.service';
import { BookingModel } from '../model/booking.model';
import { MapBookingHandler } from '../model/mapEntities/mapBookingHandler';

@Injectable()
export class DatabaseBookingRepository implements BookingRepository {
  mapper: MapBookingHandler;
  constructor(
    @InjectRepository(BookingModel)
    private readonly bookingRepository: Repository<BookingModel>,
    private logger: LoggerService,
  ) {
    this.mapper = new MapBookingHandler()
  }

  async create(booking: Omit<Booking, 'id'>): Promise<Booking> {
    try {
      const bookingModel = new BookingModel();
      bookingModel.bookingStartDate = booking.bookingStartDate;
      bookingModel.bookingEndDate = booking.bookingEndDate;
      bookingModel.bussinessUnit = booking.bussinessUnit;

      return await this.bookingRepository.save({ ...booking });
    } catch (error) {
      this.logger.error('[infrastructure] create booking', error.message);
      throw error;
    }
  }
  async findById(id: number): Promise<Booking> {
    try {
      const model = await this.bookingRepository.findOne(id);
      return this.toEntity(model);
    } catch (error) {
      this.logger.error('[infrastructure] findById booking', error.message);
      throw error;
    }
  }
  async deleteById(id: number): Promise<void> {
    try {
      await this.createQueryBuilder()
        .delete()
        .where('id = :id', { id })
        .execute();
    } catch (error) {
      this.logger.error('[infrastructure] deleteById booking', error.message);
      throw error;
    }
  }

  async getRoomsBookedByCustomerId(payload: GetRoomsBookedByCustomerIdPayload): Promise<RoomsBookedByCustomerId[]> {
    try {
      const query = this.createQueryBuilder()
        .leftJoinAndSelect('BookingModel.customer', 'customer')
        .leftJoinAndSelect('BookingModel.room', 'room')
        .leftJoinAndSelect('room.assets', 'assets')
        .where('customer.id = :customerId', { customerId: payload.customerId });

      if (payload.from) {
        query.andWhere(
          `(BookingModel.bookingStartDate >= :from OR BookingModel.bookingEndDate >= :from)`,
          { from: payload.from }
        )
      }

      if (payload.to) {
        query.andWhere(
          `(BookingModel.bookingStartDate <= :to OR BookingModel.bookingEndDate <= :to)`,
          { to: payload.to }
        )
      }

      const models = await query.getMany();
      return this.mapToRoomsBookedByCustomerId(models);
    } catch (error) {
      this.logger.error('[infrastructure] getRoomsBookedByCustomerId booking', error.message);
      throw error;
    }
  }

  async getRoomsNotAvailable(from: Date, to: Date): Promise<Booking[]> {
    try {
      const query = this.createQueryBuilder()
        .leftJoinAndSelect('BookingModel.customer', 'customer')
        .leftJoinAndSelect('BookingModel.room', 'room')
        .leftJoinAndSelect('room.assets', 'assets')
        .where(
          `((BookingModel.bookingStartDate >= :from OR BookingModel.bookingEndDate >= :from) AND
          (BookingModel.bookingStartDate <= :to OR BookingModel.bookingEndDate <= :to)) OR
          (BookingModel.bookingStartDate <= :from AND BookingModel.bookingEndDate >= :to)`,
          { from, to }
        );

      const models = await query.getMany();
      return models.map(model => this.toEntity(model));
    } catch (error) {
      this.logger.error('[infrastructure] getRoomsBookedByCustomerId booking', error.message);
      throw error;
    }
  }

  mapToRoomsBookedByCustomerId(models: BookingModel[]): RoomsBookedByCustomerId[] {
    const mapRooms = new Map<number, Room>();
    models.map(model => {
      const roomId = model.room.id;
      const room = mapRooms.get(roomId);
      if (!room) mapRooms.set(roomId, model.room);
    });

    return Array.from(mapRooms.values()).map(room => {
      return {
        id: room.id,
        name: room.name,
        floor: room.floor,
        capacity: room.capacity,
        assets: room.assets,
        bookings: models.filter(model => model.room.id === room.id).map(booking => this.toEntity(booking))
      }
    });
  }

  async isBookingAvailable(roomId: number, from: Date, to: Date): Promise<boolean> {
    try {
      const models = await this.createQueryBuilder()
        .leftJoinAndSelect('BookingModel.room', 'room')
        .where('room.id = :roomId', { roomId })
        .andWhere(`
          ((BookingModel.bookingStartDate >= :from OR BookingModel.bookingEndDate >= :from) AND
          (BookingModel.bookingStartDate <= :to OR BookingModel.bookingEndDate <= :to)) OR
          (BookingModel.bookingStartDate <= :from AND BookingModel.bookingEndDate >= :to)`,
          { from, to }
        )
        .getMany();
      return models.length === 0;
    } catch (error) {
      this.logger.error('[infrastructure] isBookingAvailable booking', error.message);
      throw error;
    }
  }

  createQueryBuilder(): SelectQueryBuilder<BookingModel> {
    return this.bookingRepository.createQueryBuilder();
  }

  toEntity(model: BookingModel): Booking {
    return this.mapper.toEntity(model);
  }
}
