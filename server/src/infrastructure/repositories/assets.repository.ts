import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Assets } from 'src/domain/entities/assets.entity';
import { AssetsRepository } from 'src/domain/repositories/assetsRepository.interface';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { LoggerService } from '../logger/logger.service';
import { AssetsModel } from '../model/assets.model';
import { MapAssetsHandler } from '../model/mapEntities/mapAssetsHandler';

@Injectable()
export class DatabaseAssetsRepository implements AssetsRepository {
    mapper: MapAssetsHandler;
    constructor(
        @InjectRepository(AssetsModel)
        private readonly assetsRepository: Repository<AssetsModel>,
        private logger: LoggerService,
    ) {
        this.mapper = new MapAssetsHandler()
    }
    async create(assets: Assets): Promise<Assets> {
        try {
            const result = await this.assetsRepository.save(assets);
            return { ...result, ...assets } as Assets;
        } catch (error) {
            this.logger.error('[infrastructure] create assets', error.message);
            throw error;
        }
    }
    async findById(id: number): Promise<Assets> {
        try {
            const model = await this.createQueryBuilder()
                .select('booking')
                .where('id = :id', { id })
                .getOne();
            return this.toEntity(model);
        } catch (error) {
            this.logger.error('[infrastructure] findById assets', error.message);
            throw error;
        }
    }
    async deleteById(id: number): Promise<void> {
        try {
            await this.createQueryBuilder()
                .delete()
                .where('id = :id', { id })
                .execute();
        } catch (error) {
            this.logger.error('[infrastructure] deleteById assets', error.message);
            throw error;
        }
    }

    createQueryBuilder(): SelectQueryBuilder<AssetsModel> {
        return this.assetsRepository.createQueryBuilder();
    }

    toEntity(model: AssetsModel): Assets {
        return this.mapper.toEntity(model);
    }
}
