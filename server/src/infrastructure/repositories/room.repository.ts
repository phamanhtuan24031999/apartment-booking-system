import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Room } from 'src/domain/entities/room.entity';
import { RoomRepository } from 'src/domain/repositories/roomRepository.interface';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { LoggerService } from '../logger/logger.service';
import { MapRoomHandler } from '../model/mapEntities/mapRoomHandler';
import { RoomModel } from '../model/room.model';

@Injectable()
export class DatabaseRoomRepository implements RoomRepository {
  mapper: MapRoomHandler;
  constructor(
    @InjectRepository(RoomModel)
    private readonly roomRepository: Repository<RoomModel>,
    private logger: LoggerService,
  ) {
    this.mapper = new MapRoomHandler()
  }

  async insert(room: Omit<Room, 'id'>): Promise<Room> {
    try {
      return await this.roomRepository.save(room);
    } catch (error) {
      this.logger.error('[infrastructure] insert room', error.message);
      throw error;
    }
  }

  async findAll(): Promise<Room[]> {
    try {
      const models = await this.createQueryBuilder()
        .leftJoinAndSelect('RoomModel.assets', 'assets',)
        .getMany();
      return models.map(model => this.toEntity(model));
    } catch (error) {
      this.logger.error('[infrastructure] findAll room', error.message);
      throw error;
    }
  }

  async findById(id: number): Promise<Room> {
    try {
      const model = await this.createQueryBuilder()
        .leftJoinAndSelect('RoomModel.assets', 'assets',)
        .where('RoomModel.id = :id', { id })
        .getOne();

      return this.toEntity(model);
    } catch (error) {
      this.logger.error('[infrastructure] findById room', error.message);
      throw error;
    }
  }

  async findByIds(ids: number[]): Promise<Room[]> {
    try {
      const models = await this.createQueryBuilder()
        .leftJoinAndSelect('RoomModel.assets', 'assets',)
        .where('RoomModel.id IN(:...ids)', { ids })
        .getMany();

      return models.map(model => this.toEntity(model));
    } catch (error) {
      this.logger.error('[infrastructure] findByIds room', error.message);
      throw error;
    }
  }

  async deleteById(id: number): Promise<void> {
    try {
      await this.createQueryBuilder()
        .delete()
        .where('id = :id', { id })
        .execute();
    } catch (error) {
      this.logger.error('[infrastructure] deleteById room', error.message);
      throw error;
    }
  }

  async updateById(id: number, room: Partial<Room>): Promise<void> {
    try {
      await this.roomRepository.update(id, room);
    } catch (error) {
      this.logger.error('[infrastructure] updateById room', error.message);
      throw error;
    }
  }

  createQueryBuilder(): SelectQueryBuilder<RoomModel> {
    return this.roomRepository.createQueryBuilder();
  }

  toEntity(model: RoomModel): Room {
    return this.mapper.toEntity(model);
  }
}
