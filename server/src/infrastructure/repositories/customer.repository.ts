import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/domain/entities/customer.entity';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { CustomerRepository } from '../../domain/repositories/customerRepository.interface';
import { LoggerService } from '../logger/logger.service';
import { CustomerModel } from '../model/customer.model';
import { MapCustomerHandler } from '../model/mapEntities/mapCustomerHandler';

@Injectable()
export class DatabaseCustomerRepository implements CustomerRepository {
  mapper: MapCustomerHandler;
  constructor(
    @InjectRepository(CustomerModel)
    private readonly customerRepository: Repository<CustomerModel>,
    private logger: LoggerService,
  ) {
    this.mapper = new MapCustomerHandler()
  }

  async create(customer: Omit<Customer, 'id'>): Promise<Customer> {
    try {
      return await this.customerRepository.save(customer)
    } catch (error) {
      this.logger.error('[infrastructure] create customer', error.message);
      throw error;
    }
  }

  async findById(id: number): Promise<Customer> {
    try {
      const model = await this.customerRepository.findOne(id);
      return this.toEntity(model);
    } catch (error) {
      this.logger.error('[infrastructure] findById customer', error.message);
      throw error;
    }
  }

  async findByIds(ids: number[]): Promise<Customer[]> {
    try {
      const models = await this.customerRepository.findByIds(ids);
      return models.map(model => this.toEntity(model));
    } catch (error) {
      this.logger.error('[infrastructure] findByIds customer', error.message);
      throw error;
    }
  }

  async deleteById(id: number): Promise<void> {
    try {
      await this.createQueryBuilder()
        .delete()
        .where('id = :id', { id })
        .execute();
    } catch (error) {
      this.logger.error('[infrastructure] deleteById customer', error.message);
      throw error;
    }
  }

  createQueryBuilder(): SelectQueryBuilder<CustomerModel> {
    return this.customerRepository.createQueryBuilder();
  }

  toEntity(model: CustomerModel): Customer {
    return this.mapper.toEntity(model);
  }
}
