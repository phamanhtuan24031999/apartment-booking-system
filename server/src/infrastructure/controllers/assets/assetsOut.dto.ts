import { ApiProperty } from '@nestjs/swagger';
import { Assets } from 'src/domain/entities/assets.entity';

export class AssetsOutDto {
    @ApiProperty()
    id: number;

    @ApiProperty()
    tv: boolean;

    @ApiProperty()
    bed: boolean;

    @ApiProperty()
    fridge: boolean;

    @ApiProperty()
    wc: boolean;

    @ApiProperty()
    kitchen: boolean;

    @ApiProperty()
    waterHeater: boolean;

    @ApiProperty()
    washingMachine: boolean;

    constructor(assets: Assets) {
        this.id = assets.id;
        this.tv = assets.tv;
        this.bed = assets.bed;
        this.fridge = assets.fridge;
        this.wc = assets.wc;
        this.kitchen = assets.kitchen;
        this.waterHeater = assets.waterHeater;
    }
}