import { Module } from '@nestjs/common';
import { UsecasesProxyModule } from '../usecases-proxy/usecases-proxy.module';
import { RoomController } from './room/room.controller';
import { CustomerController } from './customer/customer.controller';
import { BookingController } from './booking/booking.controller';

@Module({
  imports: [UsecasesProxyModule.register()],
  controllers: [RoomController, CustomerController, BookingController],
})
export class ControllersModule { }
