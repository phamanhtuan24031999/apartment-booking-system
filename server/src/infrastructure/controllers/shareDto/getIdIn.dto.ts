import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class GetIdDto {
    @Type(() => Number)
    @ApiProperty({ required: true })
    @IsNotEmpty()
    @IsNumber()
    readonly id: number;
}