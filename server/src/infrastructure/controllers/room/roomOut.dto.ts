import { ApiProperty } from '@nestjs/swagger';
import { AssetsOutDto } from '../assets/assetsOut.dto';
import { Room } from 'src/domain/entities/room.entity';

export class RoomOutDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  name: string;

  @ApiProperty()
  floor: number;

  @ApiProperty()
  capacity: number;

  @ApiProperty({ type: AssetsOutDto })
  assets: AssetsOutDto;

  @ApiProperty()
  createdDate: Date;

  @ApiProperty()
  updatedDate: Date;

  constructor(room: Room) {
    this.id = room.id
    this.name = room.name
    this.floor = room.floor
    this.capacity = room.capacity
    this.assets = new AssetsOutDto(room.assets)
    this.createdDate = room.createdDate
    this.updatedDate = room.updatedDate
  }
}
