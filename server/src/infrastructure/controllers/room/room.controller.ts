import { Body, Controller, Delete, Get, Inject, Param, Patch, Post } from '@nestjs/common';
import { ApiOkResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AddRoomUseCases, DeleteRoomUseCases, GetRoomUseCases, GetRoomsUseCases, UpdateRoomUseCases } from 'src/usecases/room/room.usecases';
import { UseCaseProxy } from '../../usecases-proxy/usecases-proxy';
import { UsecasesProxyModule } from '../../usecases-proxy/usecases-proxy.module';
import { GetIdDto } from '../shareDto/getIdIn.dto';
import { AddRoomInDto, UpdateRoomInDto } from './roomIn.dto';
import { RoomOutDto } from './roomOut.dto';

@Controller('room')
@ApiTags('room')
@ApiResponse({ status: 500, description: 'Internal error' })
export class RoomController {
  constructor(
    @Inject(UsecasesProxyModule.GET_ROOM_USECASES_PROXY)
    private readonly getRoomUsecaseProxy: UseCaseProxy<GetRoomUseCases>,
    @Inject(UsecasesProxyModule.GET_ROOMS_USECASES_PROXY)
    private readonly getRoomsUsecaseProxy: UseCaseProxy<GetRoomsUseCases>,
    @Inject(UsecasesProxyModule.DELETE_ROOM_USECASES_PROXY)
    private readonly deleteRoomUsecaseProxy: UseCaseProxy<DeleteRoomUseCases>,
    @Inject(UsecasesProxyModule.POST_ROOM_USECASES_PROXY)
    private readonly addRoomsUsecaseProxy: UseCaseProxy<AddRoomUseCases>,
    @Inject(UsecasesProxyModule.PATCH_ROOM_USECASES_PROXY)
    private readonly updateRoomsUsecaseProxy: UseCaseProxy<UpdateRoomUseCases>,
  ) { }

  @Get(':id')
  @ApiOkResponse({ type: RoomOutDto })
  async getRoom(@Param() { id }: GetIdDto) {
    const room = await this.getRoomUsecaseProxy.getInstance().execute(id);
    return new RoomOutDto(room);
  }

  @Get()
  @ApiOkResponse({ type: RoomOutDto })
  async getRooms() {
    const rooms = await this.getRoomsUsecaseProxy.getInstance().execute();
    return rooms.map(room => new RoomOutDto(room));
  }

  @Delete(':id')
  @ApiOkResponse()
  async deleteRoom(@Param() { id }: GetIdDto) {
    await this.deleteRoomUsecaseProxy.getInstance().execute(id);
    return 'success';
  }

  @Post()
  @ApiOkResponse({ type: RoomOutDto })
  async createRoom(@Body() addRoomDto: AddRoomInDto) {
    const roomCreated = await this.addRoomsUsecaseProxy.getInstance().execute(addRoomDto);
    return new RoomOutDto(roomCreated);
  }

  @Patch()
  @ApiOkResponse()
  async updateRoom(@Body() addRoomDto: UpdateRoomInDto) {
    const roomCreated = await this.updateRoomsUsecaseProxy.getInstance().execute(addRoomDto);
    return 'success';
  }
}
