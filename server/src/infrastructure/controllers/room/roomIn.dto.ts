import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsBoolean, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class AddRoomInDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsString()
  readonly name: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsNumber()
  readonly floor: number;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsNumber()
  readonly capacity: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsBoolean()
  readonly tv: boolean;

  @ApiPropertyOptional()
  @IsOptional()
  @IsBoolean()
  readonly bed: boolean;

  @ApiPropertyOptional()
  @IsOptional()
  @IsBoolean()
  readonly fridge: boolean;

  @ApiPropertyOptional()
  @IsOptional()
  @IsBoolean()
  readonly wc: boolean;

  @ApiPropertyOptional()
  @IsOptional()
  @IsBoolean()
  readonly kitchen: boolean;

  @ApiPropertyOptional()
  @IsOptional()
  @IsBoolean()
  readonly waterHeater: boolean;

  @ApiPropertyOptional()
  @IsOptional()
  @IsBoolean()
  readonly washingMachine: boolean;
}

export class UpdateRoomInDto {
  @Type(() => Number)
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  readonly id: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly name?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  readonly floor?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  readonly capacity?: number;
}
