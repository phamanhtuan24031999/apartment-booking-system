import { ApiProperty } from '@nestjs/swagger';
import { Customer } from 'src/domain/entities/customer.entity';

export class CustomerOutDto {
    @ApiProperty()
    id: number;

    @ApiProperty()
    firstName: string;

    @ApiProperty()
    lastName: string;

    @ApiProperty()
    email: string;

    @ApiProperty()
    phone: string;

    @ApiProperty()
    createdDate: Date;

    @ApiProperty()
    updatedDate: Date;

    constructor(customer: Customer) {
        this.id = customer.id;
        this.firstName = customer.firstName;
        this.lastName = customer.lastName;
        this.email = customer.email;
        this.phone = customer.phone;
        this.createdDate = customer.createdDate;
        this.updatedDate = customer.updatedDate;
    }
}