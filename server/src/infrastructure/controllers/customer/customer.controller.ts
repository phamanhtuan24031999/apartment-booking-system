import { Body, Controller, Delete, Get, Inject, Param, Post } from '@nestjs/common';
import { ApiOkResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateCustomerUseCases, GetCustomerUseCases } from 'src/usecases/customer/customer.usecase';
import { ApiResponseType } from '../../common/swagger/response.decorator';
import { UseCaseProxy } from '../../usecases-proxy/usecases-proxy';
import { UsecasesProxyModule } from '../../usecases-proxy/usecases-proxy.module';
import { GetIdDto } from '../shareDto/getIdIn.dto';
import { CreateCustomerInDto } from './customerIn.dto';
import { CustomerOutDto } from './customerOut.dto';
import { DeleteRoomUseCases } from 'src/usecases/room/room.usecases';

@Controller('customer')
@ApiTags('customer')
@ApiResponse({ status: 500, description: 'Internal error' })
export class CustomerController {
  constructor(
    @Inject(UsecasesProxyModule.GET_CUSTOMER_USECASES_PROXY)
    private readonly getCustomerUsecaseProxy: UseCaseProxy<GetCustomerUseCases>,
    @Inject(UsecasesProxyModule.POST_CUSTOMER_USECASES_PROXY)
    private readonly createCustomerUsecaseProxy: UseCaseProxy<CreateCustomerUseCases>,
    @Inject(UsecasesProxyModule.DELETE_CUSTOMER_USECASES_PROXY)
    private readonly deleteCustomerUsecaseProxy: UseCaseProxy<DeleteRoomUseCases>,
  ) { }

  @Get(':id')
  @ApiOkResponse({ type: CustomerOutDto })
  async getRoom(@Param() { id }: GetIdDto) {
    const customer = await this.getCustomerUsecaseProxy.getInstance().execute(id);
    return new CustomerOutDto(customer);
  }

  @Delete(':id')
  @ApiOkResponse()
  async deleteRoom(@Param() { id }: GetIdDto) {
    await this.deleteCustomerUsecaseProxy.getInstance().execute(id);
    return 'success';
  }

  @Post()
  @ApiResponseType(CustomerOutDto)
  async createRoom(@Body() addRoomDto: CreateCustomerInDto) {
    const roomCreated = await this.createCustomerUsecaseProxy.getInstance().execute(addRoomDto);
    return new CustomerOutDto(roomCreated);
  }
}
