import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateCustomerInDto {
    @ApiProperty({ required: true })
    @IsNotEmpty()
    @IsString()
    readonly firstName: string;

    @ApiProperty({ required: true })
    @IsNotEmpty()
    @IsString()
    readonly lastName: string;

    @ApiProperty({ required: true })
    @IsNotEmpty()
    @IsString()
    readonly email: string;

    @ApiProperty({ required: true })
    @IsNotEmpty()
    @IsString()
    readonly phone: string;
}

