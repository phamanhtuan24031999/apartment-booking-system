import { ApiProperty } from '@nestjs/swagger';
import { Booking } from 'src/domain/entities/booking.entity';
import { RoomsBookedByCustomerId } from 'src/domain/repositories/bookingRepository.interface';
import { RoomsAvailable } from 'src/usecases/booking/getRoomsAvailable.usecases';
import { AssetsOutDto } from '../assets/assetsOut.dto';
import { RoomOutDto } from '../room/roomOut.dto';

export class BookingOutDto {
    @ApiProperty()
    id: number;

    @ApiProperty()
    roomId: number;

    @ApiProperty()
    customerId: number;

    @ApiProperty()
    bookingStartDate: Date;

    @ApiProperty()
    bookingEndDate: Date;

    @ApiProperty()
    purpose: string;

    @ApiProperty()
    bussinessUnit: string;

    constructor(booking: Booking) {
        this.id = booking.id;
        this.roomId = booking.room.id;
        this.customerId = booking.customer.id;
        this.bookingStartDate = booking.bookingStartDate;
        this.bookingEndDate = booking.bookingEndDate;
        this.purpose = booking.purpose;
        this.bussinessUnit = booking.bussinessUnit;
    }
}

export class RoomsBookedByCustomerIdDto {
    @ApiProperty()
    id: number;

    @ApiProperty()
    name: string;

    @ApiProperty()
    floor: number;

    @ApiProperty()
    capacity: number;

    @ApiProperty()
    assets: AssetsOutDto

    @ApiProperty()
    bookings: BookingOutDto[]

    constructor(room: RoomsBookedByCustomerId) {
        this.id = room.id;
        this.name = room.name;
        this.floor = room.floor;
        this.capacity = room.capacity;
        this.assets = new AssetsOutDto(room.assets);
        this.bookings = room.bookings.map(booking => new BookingOutDto(booking))
    }
}

export class RoomsAvailableDto {
    @ApiProperty()
    day: Date;

    @ApiProperty()
    rooms: RoomOutDto[];

    constructor(room: RoomsAvailable) {
        this.day = room.day;
        this.rooms = room.rooms.map(r => new RoomOutDto(r))
    }
}
