import { Body, Controller, Get, Inject, Param, Post, Query } from '@nestjs/common';
import { ApiOkResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { BookingRoomsUseCases } from 'src/usecases/booking/bookingRooms.usecases';
import { GetRoomsBookedByCustomerIdUseCases } from 'src/usecases/booking/getRoomsBookedByCustomerId.usecases';
import { UseCaseProxy } from '../../usecases-proxy/usecases-proxy';
import { UsecasesProxyModule } from '../../usecases-proxy/usecases-proxy.module';
import { BookingMultiInDto, GetCustomerIdInDto, GetRoomsAvailableInDto, GetRoomsBookedByCustomerIdInDto } from './bookingIn.dto';
import { RoomsAvailableDto, RoomsBookedByCustomerIdDto } from './bookingOut.dto';
import { GetRoomsAvailableUseCases } from 'src/usecases/booking/getRoomsAvailable.usecases';

@Controller('booking')
@ApiTags('booking')
@ApiResponse({ status: 500, description: 'Internal error' })
export class BookingController {
  constructor(
    @Inject(UsecasesProxyModule.BOOKING_ROOM_USECASES_PROXY)
    private readonly bookingRoomUseCases: UseCaseProxy<BookingRoomsUseCases>,
    @Inject(UsecasesProxyModule.GET_ROOM_BOOKED_BY_CUSTOMERID_USECASES_PROXY)
    private readonly getRoomsBookedByCustomerIdUseCases: UseCaseProxy<GetRoomsBookedByCustomerIdUseCases>,
    @Inject(UsecasesProxyModule.GET_ROOMS_AVAILABLE_USECASES_PROXY)
    private readonly getRoomsAvailableUseCases: UseCaseProxy<GetRoomsAvailableUseCases>,
  ) { }

  @Get(':customerId')
  @ApiOkResponse({ type: RoomsBookedByCustomerIdDto })
  async getRoom(@Param() { customerId }: GetCustomerIdInDto, @Query() payload: GetRoomsBookedByCustomerIdInDto) {
    const rooms = await this.getRoomsBookedByCustomerIdUseCases.getInstance().execute({ ...payload, customerId });
    return rooms.map(room => new RoomsBookedByCustomerIdDto(room));
  }

  @Get('/available/rooms')
  @ApiOkResponse({ type: [RoomsAvailableDto] })
  async getRoomsAvailable(@Query() payload: GetRoomsAvailableInDto) {
    const rooms = await this.getRoomsAvailableUseCases.getInstance().execute(payload);
    return rooms.map(room => new RoomsAvailableDto(room));
  }

  @Post()
  @ApiOkResponse()
  async bookingRoom(@Body() bookingRoomsDto: BookingMultiInDto) {
    await this.bookingRoomUseCases.getInstance().execute(bookingRoomsDto.items);
    return 'success';
  }
}
