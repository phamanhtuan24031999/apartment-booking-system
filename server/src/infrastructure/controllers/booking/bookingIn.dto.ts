import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, IsBoolean, IsDate, IsNotEmpty, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';

export class BookingInDto {
    @Type(() => Number)
    @ApiProperty({ required: true })
    @IsNotEmpty()
    @IsNumber()
    readonly roomId: number;

    @Type(() => Number)
    @ApiProperty({ required: true })
    @IsNotEmpty()
    @IsNumber()
    readonly customerId: number;

    @Type(() => Date)
    @ApiProperty({ required: true })
    @IsNotEmpty()
    @IsDate()
    readonly bookingStartDate: Date;

    @Type(() => Date)
    @ApiProperty({ required: true })
    @IsNotEmpty()
    @IsDate()
    readonly bookingEndDate: Date;

    @Type(() => String)
    @ApiProperty({ required: true })
    @IsNotEmpty()
    @IsString()
    readonly purpose: string;

    @Type(() => String)
    @ApiProperty({ required: true })
    @IsNotEmpty()
    @IsString()
    readonly bussinessUnit: string;
}

export class BookingMultiInDto {
    @ValidateNested({ each: true })
    @Type(() => BookingInDto)
    @IsArray()
    readonly items: BookingInDto[];
}

export class GetRoomsBookedByCustomerIdInDto {
    @Type(() => Date)
    @ApiPropertyOptional()
    @IsOptional()
    @IsDate()
    readonly from?: Date;

    @Type(() => Date)
    @ApiPropertyOptional()
    @IsOptional()
    @IsDate()
    readonly to?: Date;
}

export class GetRoomsAvailableInDto {
    @Type(() => Date)
    @ApiProperty({ required: true })
    @IsNotEmpty()
    @IsDate()
    readonly from: Date;

    @Type(() => Date)
    @ApiProperty({ required: true })
    @IsNotEmpty()
    @IsDate()
    readonly to: Date;
}

export class GetCustomerIdInDto {
    @Type(() => Number)
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    readonly customerId: number;
}