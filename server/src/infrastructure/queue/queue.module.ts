import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { MessageProducerService } from './queue.service';

@Module({
    imports: [
        BullModule.forRoot({
            redis: {
                host: 'localhost',
                port: 6379,
            },
        }),
        BullModule.registerQueue({
            name: 'message-queue'
        }),
    ],
    providers: [MessageProducerService]
})
export class QueueModule { }