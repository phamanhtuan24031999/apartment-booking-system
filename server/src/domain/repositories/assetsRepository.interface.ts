import { Assets } from '../entities/assets.entity';

export interface AssetsRepository {
  create(assets: Omit<Assets, 'id'>): Promise<Assets>;
  findById(id: number): Promise<Assets>;
  deleteById(id: number): Promise<void>;
}
