import { Room } from '../entities/room.entity';

export interface RoomRepository {
  insert(rome: Omit<Room, 'id'>): Promise<Room>;
  findAll(): Promise<Room[]>;
  findById(id: number): Promise<Room>;
  findByIds(ids: number[]): Promise<Room[]>;
  deleteById(id: number): Promise<void>;
  updateById(id: number, room: Partial<Room>): Promise<void>;

}
