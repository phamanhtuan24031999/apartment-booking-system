import { Assets } from '../entities/assets.entity';
import { Booking } from '../entities/booking.entity';

export interface GetRoomsBookedByCustomerIdPayload {
  customerId: number;
  from?: Date;
  to?: Date;
}

export interface RoomsBookedByCustomerId {
  id: number;
  name: string;
  floor: number;
  capacity: number;
  assets: Assets;
  bookings: Booking[];
}

export interface BookingRepository {
  create(booking: Omit<Booking, 'id'>): Promise<Booking>;
  findById(id: number): Promise<Booking>;
  deleteById(id: number): Promise<void>;
  getRoomsBookedByCustomerId(payload: GetRoomsBookedByCustomerIdPayload): Promise<RoomsBookedByCustomerId[]>;
  isBookingAvailable(roomId: number, from: Date, to: Date): Promise<boolean>;
  getRoomsNotAvailable(from: Date, to: Date): Promise<Booking[]>
}
