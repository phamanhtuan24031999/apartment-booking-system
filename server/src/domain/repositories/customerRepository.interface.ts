import { Customer } from '../entities/customer.entity';

export interface CustomerRepository {
  create(customer: Omit<Customer, 'id'>): Promise<Customer>;
  findById(id: number): Promise<Customer>;
  findByIds(ids: number[]): Promise<Customer[]>;
  deleteById(id: number): Promise<void>;
}
