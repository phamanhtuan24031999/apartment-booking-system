export class Assets {
    id: number;
    tv?: boolean;
    bed?: boolean;
    fridge?: boolean;
    wc?: boolean;
    kitchen?: boolean;
    waterHeater?: boolean;
    washingMachine?: boolean;
}
