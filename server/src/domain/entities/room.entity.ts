import { Assets } from "./assets.entity";

export class Room {
  id: number;
  name: string;
  floor: number;
  capacity: number;
  assets: Assets;

  createdDate?: Date;
  updatedDate?: Date;
}
