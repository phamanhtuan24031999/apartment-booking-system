import { Customer } from "./customer.entity";
import { Room } from "./room.entity";

export class Booking {
    id: number;
    room: Room;
    customer: Customer;

    bookingStartDate: Date;
    bookingEndDate: Date;
    purpose: string;
    bussinessUnit: string
}
