export class Customer {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;

  createdDate?: Date;
  updatedDate?: Date;
}

