## Aparment Booking System
- customer can booking multiple rooms for a number of day
- customer can see rooms that they booked
- customer can see rooms available ​for any particular day in the future
- staff can create new room
- staff can create new customer
- time to check in and check out are in the same day (it mean when you book room in 2023-8-11 you will checkin at 00h00 and checkout out 23h59)
- apartment have no branch
- payment and deposit that we not handle here for simple reason

## Installation

```bash
$ npm install
```

## Postgres

Install docker and run the command:

```bash
cd docker
docker compose -f "docker\docker-compose.yml" up -d --build
```

run migration

```bash
# to init migration (just for development)
npm run typeorm:generate:win -n init

# run migration to apply to database
npm run typeorm:run:win

# drop database (just for development)
npm run typeorm:win schema:drop
```

## Running the app

```bash
cd /server

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Import postman collection

```
# development
Import file in folder postman
```

## Seeding data

```
# development
Import file in folder sql
```

## Curl to test
### create room
```bash
curl -X 'POST' \
  'http://localhost:3000/api_v1/room' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "r102",
  "floor": 1,
  "capacity": 2,
  "tv": true,
  "bed": true,
  "fridge": true,
  "wc": true,
  "kitchen": true,
  "waterHeater": false,
  "washingMachine": false
}'
```

### create customer
```bash
curl -X 'POST' \
  'http://localhost:3000/api_v1/customer' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "firstName": "tuan222",
  "lastName": "anh333",
  "email": "tuan@gmail.com",
  "phone": "0944734502"
}'
```

### booking room
```bash
curl -X 'POST' \
  'http://localhost:3000/api_v1/booking' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{
    "items": [
        {
            "roomId": 6,
            "customerId": 2,
            "bookingStartDate": "2023-8-22",
            "bookingEndDate": "2023-8-23",
            "purpose": "boot camp",
            "bussinessUnit": "company cargrill"
        }
    ]
}'
```

### get room booked by customerId
```bash
curl -X 'GET' \
  'http://localhost:3000/api_v1/booking/2?from=2023-8-18&to=2023-8-24' \
  -H 'accept: application/json'
```

### get room available
```bash
curl -X 'GET' \
  'http://localhost:3000/api_v1/booking/available/rooms?from=2023-8-18&to=2023-8-24' \
  -H 'accept: application/json'
```

## points that can be further improved
- write unit test, intergration test, e2e test to make sure high quality and reliable of software
- make shoter and cleaner file usecase proxy
- implement a rate limiter if the apartment is growing up
- implement queue for booking room to make sure no customer can book a same room at the same time
- create cursor pagination for api get room available and get room booked
- create payment method and authentication
